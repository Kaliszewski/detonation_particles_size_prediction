import matplotlib.pyplot as plt
import configparser
import dataformatter as df

class DataPlotter:
    
    train_data = []

    def __init__(self, features, non_categorical_fetures, target):
        self.config = configparser.ConfigParser()
        self.config.read('props.ini')
        self.features = features
        self.non_categorical_fetures = non_categorical_fetures
        self.target = target

    def plot_per_feature(self):
        if len(self.non_categorical_fetures) > 0 and len(self.target) > 0:
            target_title = self.config['train_data']['target']
            non_cat_feature_labels = list(self.config['train_data']['non_categorical_features'].split(','))
            for idx, feature in enumerate(self.non_categorical_fetures):
                feature_title = non_cat_feature_labels[idx]
                plt.scatter(feature,self.target)
                plt.title(target_title + "per" + feature_title)
                plt.xlabel(feature_title)
                plt.ylabel(target_title)
                plt.autoscale(tight=True)
                plt.grid()
                plt.show()

    def plot_per_category(self, category_data_dict):
        if len(category_data_dict) > 0:
            target = self.config['train_data']['target']
            features = list(self.config['train_data']['non_categorical_features'].split(','))
            i = 1
            plots_nr = len(category_data_dict.items()) * len(features)
            for feature in features:
                for key, value in category_data_dict.items():
                    plt.subplot(2, 3, i)
                    plt.plot(value[feature],value[target], 'ro')
                    plt.title(key)
                    plt.xlabel(feature)
                    plt.ylabel(target)
                    plt.autoscale(tight=True)
                    plt.grid()
                    i+=1
            mng = plt.get_current_fig_manager()
            mng.full_screen_toggle() 
            plt.show()